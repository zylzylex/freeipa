#
# Copyright (C) 2016  FreeIPA Contributors see COPYING for license
#

# pylint: disable=unicode-builtin

from __future__ import print_function

import pytest
from uuid import uuid4

from ipalib import errors
from ipatests.test_xmlrpc.tracker.group_plugin import GroupTracker
from ipatests.test_xmlrpc.tracker.host_plugin import HostTracker
from ipatests.test_xmlrpc.tracker.hostgroup_plugin import HostGroupTracker
from ipatests.test_xmlrpc.tracker.privilege_plugin import PrivilegeTracker
from ipatests.test_xmlrpc.tracker.role_plugin import RoleTracker
from ipatests.test_xmlrpc.tracker.user_plugin import UserTracker
from ipatests.util import assert_deepequal
from xmlrpc_test import (
    raises_exact,
    XMLRPC_test,
)

_DESCRIPTION = u'Description string'
_NEW_DESC = u'New description'
_INVALID_NAME = u'  Invalid Name  '


@pytest.fixture(scope='class')
def role(request):
    """Default testing role."""
    tracker = RoleTracker(name=u'test-role1', description=_DESCRIPTION)
    return tracker.make_fixture(request)


@pytest.fixture(scope='class')
def role2(request):
    """Additional testing role."""
    tracker = RoleTracker(name=u'test-role2', description=_DESCRIPTION)
    return tracker.make_fixture(request)


@pytest.fixture(scope='class')
def random_role(request):
    """Testing role with random name."""
    tracker = RoleTracker(
        name=unicode(uuid4()), description=_DESCRIPTION
    )
    return tracker.make_fixture(request)


@pytest.fixture(scope='class')
def invalid_role(request):
    """Role with invalid name."""
    tracker = RoleTracker(name=_INVALID_NAME)
    return tracker.make_fixture(request)


@pytest.fixture(scope='class')
def user(request):
    """Default testing user."""
    tracker = UserTracker(name=u'user', givenname=u'UserGN', sn=u'UserSN')
    return tracker.make_fixture(request)


@pytest.fixture(scope='class')
def group(request):
    """Default testing group."""
    tracker = GroupTracker(name=u'group1', description=u'Group description')
    return tracker.make_fixture(request)


@pytest.fixture(scope='class')
def host(request):
    """Default testing host."""
    tracker = HostTracker(name=u'testhost')
    return tracker.make_fixture(request)


@pytest.fixture(scope='class')
def hostgroup(request):
    """Default testing hostgroup."""
    tracker = HostGroupTracker(name=u'hostgroup')
    return tracker.make_fixture(request)


@pytest.fixture(scope='class')
def privilege(request):
    """Default testing privilege."""
    tracker = PrivilegeTracker(name=u'privilege1')
    return tracker.make_fixture(request)


@pytest.mark.tier1
class TestNonexistentRole(XMLRPC_test):
    """Test for correct handling of operations on nonexisting role."""
    def test_retrieve(self, random_role):
        """Try to retrieve non-existent role."""
        random_role.ensure_missing()
        command = random_role.make_retrieve_command()
        with raises_exact(errors.NotFound(
                reason=u'%s: role not found' % random_role.cn)):
            command()

    def test_update(self, random_role):
        """Try to update non-existent role."""
        command = random_role.make_update_command(
            updates={'description': u'Foo'})
        with raises_exact(errors.NotFound(
                reason=u'%s: role not found' % random_role.cn)):
            command()

    def test_delete(self, random_role):
        """Try to delete non-existent role."""
        command = random_role.make_delete_command()
        with raises_exact(errors.NotFound(
                reason=u'%s: role not found' % random_role.cn)):
            command()

    def test_rename(self, random_role):
        """Try to rename non-existent role."""
        new_name = uuid4()
        command = random_role.make_update_command(
            updates={'setattr': u'cn=%s' % new_name})
        with raises_exact(errors.NotFound(
                reason=u'%s: role not found' % random_role.cn)):
            command()

    def test_find(self, random_role):
        """Try to find non-existent role."""
        result = random_role.find(random_role.cn)
        expected = RoleTracker.find_tracker_roles([random_role])
        assert_deepequal(expected, result)

    def test_create_invalid(self, invalid_role):
        """Try to create role with an invalid name."""
        command = invalid_role.make_create_command()
        with raises_exact(
                errors.ValidationError(
                    name='name',
                    error=u'Leading and trailing spaces are not allowed')):
            command()

    def test_add_privilege(self, random_role, privilege):
        """Try to add a privilege to non-existing role."""
        privilege.ensure_exists()
        command = random_role.make_command(
            'role_add_privilege', random_role.cn, privilege=privilege.cn)
        with raises_exact(errors.NotFound(
                reason=u'%s: role not found' % random_role.cn)):
            command()

    def test_remove_privileges(self, random_role, privilege):
        """Try to remove privilege from non-existent role."""
        privilege.ensure_exists()
        command = random_role.make_command(
            'role_remove_privilege', random_role.cn, privilege=privilege.cn)
        with raises_exact(errors.NotFound(
                reason=u'%s: role not found' % random_role.cn)):
            command()

    def test_add_member(self, random_role, group):
        """Try to add a member to non-existing role."""
        group.ensure_exists()
        command = random_role.make_command(
            'role_add_member', random_role.cn, group=group.cn)
        with raises_exact(errors.NotFound(
                reason=u'%s: role not found' % random_role.cn)):
            command()

    def test_remove_member(self, random_role, group):
        """Try to remove member from non-existing role."""
        group.ensure_exists()
        command = random_role.make_command(
            'role_remove_member', random_role.cn, group=group.cn)
        with raises_exact(errors.NotFound(
                reason=u'%s: role not found' % random_role.cn)):
            command()


@pytest.mark.tier1
class TestDeletedRole(XMLRPC_test):
    """Test for correct handling of operations on deleted role."""

    def setUpClass(self, role):
        """Create and delete role."""
        role.ensure_exists()
        role.ensure_missing()

    def test_retrieve(self, role):
        """Try to retrieve deleted role."""
        command = role.make_retrieve_command()
        with raises_exact(errors.NotFound(
                reason=u'%s: role not found' % role.cn)):
            command()

    def test_update(self, role):
        """Try to update deleted role."""
        command = role.make_update_command(updates={'description': u'Foo'})
        with raises_exact(errors.NotFound(
                reason=u'%s: role not found' % role.cn)):
            command()

    def test_delete(self, role):
        """Try to remove deleted role."""
        command = role.make_delete_command()
        with raises_exact(errors.NotFound(
                reason=u'%s: role not found' % role.cn)):
            command()

    def test_rename(self, role):
        """Try to rename deleted role."""
        new_name = uuid4()
        command = role.make_update_command(
            updates={'setattr': u'cn=%s' % new_name})
        with raises_exact(errors.NotFound(
                reason=u'%s: role not found' % role.cn)):
            command()

    def test_find(self, role):
        """Try to search for deleted role."""
        result = role.find(role.cn)
        expected = RoleTracker.find_tracker_roles([role])
        assert_deepequal(expected, result)

    def test_add_privilege(self, role, privilege):
        """Try to add a privilege to deleted role."""
        privilege.ensure_exists()
        command = role.make_command(
            'role_add_privilege', role.cn, privilege=privilege.cn)
        with raises_exact(errors.NotFound(
                reason=u'%s: role not found' % role.cn)):
            command()

    def test_remove_privileges(self, role, privilege):
        """Try to remove privilege from deleted role."""
        privilege.ensure_exists()
        command = role.make_command(
            'role_remove_privilege', role.cn, privilege=privilege.cn)
        with raises_exact(errors.NotFound(
                reason=u'%s: role not found' % role.cn)):
            command()

    def test_add_member(self, role, group):
        """Try to add a member to deleted role."""
        group.ensure_exists()
        command = role.make_command(
            'role_add_member', role.cn, group=group.cn)
        with raises_exact(errors.NotFound(
                reason=u'%s: role not found' % role.cn)):
            command()

    def test_remove_member(self, role, group):
        """Try to remove member from deleted role."""
        group.ensure_exists()
        command = role.make_command(
            'role_remove_member', role.cn, group=group.cn)
        with raises_exact(errors.NotFound(
                reason=u'%s: role not found' % role.cn)):
            command()


@pytest.mark.tier1
class TestRoleBasic(XMLRPC_test):
    """Test basic functionality of role plugin."""

    def test_create_retrieve(self, role):
        """Create and retrieve role, with checks performed in role_plugin."""
        role.ensure_exists()
        result = role.retrieve()
        role.check_retrieve(result)

    def test_retrieve_all(self, role):
        """Test role-show command with --all parameter."""
        result = role.retrieve(all=True)
        role.check_retrieve(result, all=True)

    def test_create_duplicate(self, role):
        """Make sure it is not possible to create duplicate role."""
        role.ensure_exists()
        command = role.make_create_command()
        with raises_exact(
                errors.DuplicateEntry(
                    message=u'role with name "%s" already exists' % role.cn)):
            command()

    def test_delete(self, role):
        """Delete role and make sure we can not retrieve it.

        All checks are performed in tracker.role_plugin.RoleTracker class.
        """
        role.ensure_exists()
        role.delete()


@pytest.mark.tier1
class TestRoleUpdate(XMLRPC_test):
    """Class for testing ipa role-mod command."""
    def test_update_description(self, role):
        """Test updating role description."""
        role.ensure_exists()
        result = role.update({'description': _NEW_DESC})
        role.update_tracker({'description': _NEW_DESC})
        role.check_update(result)

    def test_rename(self, role):
        """Rename role to random identifier."""
        new_name = unicode(uuid4())
        result = role.update({'rename': new_name})
        role.update_tracker({'rename': new_name})
        role.check_update(result)
        # we additionaly have to set cn for further testing
        role.cn = new_name

    def test_rename_again(self, role):
        """Try to rename role to a same name."""
        with raises_exact(errors.EmptyModlist(
                message=u'no modifications to be performed')):
            role.update({'rename': role.cn})

    def test_rename_to_existing(self, role, random_role):
        """Try to rename role to already taken name."""
        random_role.ensure_exists()
        with raises_exact(errors.DuplicateEntry(
                message=u'This entry already exists')):
            role.update({'rename': random_role.cn})

    def test_rename_to_invalid(self, role):
        """Try to rename role to invalid name."""
        with raises_exact(errors.ValidationError(
                message=(u"invalid 'rename': "
                         u"Leading and trailing spaces are not allowed"))):
            role.update({'rename': _INVALID_NAME})

    def test_retrieve(self, role):
        """Check that previous tests didn't change role's state."""
        result = role.retrieve()
        role.check_retrieve(result)


@pytest.mark.tier1
class TestRolePrivileges(XMLRPC_test):
    """Test handling of privileges in roles."""

    def test_add_privilege_to_role(self, role, privilege):
        """Add valid privilege to an existing role."""
        role.ensure_exists()
        privilege.ensure_exists()
        result = role.add_privilege(privilege.cn)
        role.check_add_privilege(result)

    def test_add_privilege_to_role_again(self, role, privilege):
        """Try to add privilege to role again."""
        result = role.add_privilege(privilege.cn)
        role.check_add_duplicate_privilege(result, privilege.cn)

    def test_add_zero_privilege_to_role(self, role):
        """Add empty privilege to role and check the result."""
        result = role.add_privilege(None)
        role.check_add_zero_privilege(result)

    def test_remove_zero_privilege_from_role(self, role):
        """Remove empty privilege from role and check result."""
        result = role.remove_privilege(None)
        role.check_remove_zero_privilege(result)

    def test_show_updates(self, role, privilege):
        """Ensure that role-show command returns correct result."""
        result = role.retrieve()
        role.check_retrieve(result)

    def test_show_all_updates(self, role, privilege):
        """Ensure that role-show --all command returns correct result."""
        result = role.retrieve(all=True)
        role.check_retrieve(result, all=True)

    def test_remove_privilege_from_role(self, role, privilege):
        """Remove privilege from role."""
        result = role.remove_privilege(privilege.cn)
        role.check_remove_privilege(result)

    def test_remove_privilege_from_role_again(self, role, privilege):
        """Try to remove already removed privilege from role."""
        result = role.remove_privilege(privilege.cn)
        role.check_remove_privilege_false(result, privilege.cn)

    def test_remove_privilege(self, role, privilege):
        """Test physical removal of privilige from system.

        Add privilege to the role, remove privilige from system and check that
        it is not present in role-show command. This test assumes that
        test_add_privilige_to_role holds true.
        """
        privilege.ensure_exists()
        role.add_privilege(privilege.cn)
        privilege.ensure_missing()
        role.update_tracker({u'memberof_privilege': privilege.cn}, remove=True)
        result = role.retrieve()
        role.check_retrieve(result)

    def test_add_nonexistent_privilege_to_role(self, role):
        """Try to add non-existent privilege to role."""
        priv_name = unicode(str(uuid4()))
        result = role.add_privilege(priv_name)
        # we have to update tracker manually here since it was modified
        role.update_tracker({u'memberof_privilege': priv_name}, remove=True)
        role.check_handle_nonexistent_privilege(result, priv_name)

    def test_remove_nonexistent_privilege_from_role(self, role):
        """Try to remove non-existent privilege from role."""
        priv_name = unicode(str(uuid4()))
        result = role.remove_privilege(priv_name)
        role.check_handle_nonexistent_privilege(result, priv_name)


class TestRoleMembers(XMLRPC_test):
    """Test member related operations on roles."""

    def test_add_group_to_role(self, group, role):
        """Add new group to the role."""
        group.ensure_exists()
        role.ensure_exists()
        result = role.add_member(group.cn, member_type='group')
        role.check_add_member(result)

    def test_add_user_to_role(self, user, role):
        """Add new user to the role."""
        user.ensure_exists()
        result = role.add_member(user.uid, member_type='user')
        role.check_add_member(result)

    def test_add_host_to_role(self, host, role):
        """Add new user to the role."""
        host.ensure_exists()
        result = role.add_member(host.fqdn, member_type='host')
        role.check_add_member(result)

    def test_add_hostgroup_to_role(self, hostgroup, role):
        """Add new user to the role."""
        hostgroup.ensure_exists()
        result = role.add_member(hostgroup.cn, member_type='hostgroup')
        role.check_add_member(result)

    def test_add_duplicate_member_to_role(self, group, role):
        """Try to add duplicate group to the role."""
        result = role.add_member(group.cn, member_type='group')
        role.check_add_duplicate_member(result, group.cn, member_type='group')

    def test_remove_user_from_role(self, user, role):
        """Remove user from role and check results."""
        result = role.remove_member(user.uid, member_type='user')
        role.check_remove_member(result)

    def test_show_remove_user(self, role):
        """Check that user was removed from role."""
        result = role.retrieve()
        role.check_retrieve(result)

    def test_remove_user_from_role_again(self, user, role):
        """Try to remove user from the role again and check results."""
        result = role.remove_member(user.uid, member_type='user')
        role.check_remove_member_false(result, user.uid, member_type='user')

    def test_remove_group_from_role(self, group, role):
        """Remove group from role and check results."""
        result = role.remove_member(group.cn, member_type='group')
        role.check_remove_member(result)

    def test_remove_host_from_role(self, host, role):
        """Remove host from role and check results."""
        result = role.remove_member(host.fqdn, member_type='host')
        role.check_remove_member(result)

    def test_remove_hostgroup_from_role(self, hostgroup, role):
        """Remove hostgroup from role and check results."""
        result = role.remove_member(hostgroup.cn, member_type='hostgroup')
        role.check_remove_member(result)

    def test_remove_member(self, group, role):
        """Test physical removal of role's group member.

        Remove group from the system and check it is not present
        in role-show command. This test assumes that test_add_group_to_role
        from this class and tests in test_group_plugin file hold true.
        """
        role.ensure_exists()
        role.add_member(group.cn, member_type='group')
        group.ensure_missing()
        # we need to update tracker manually
        role.update_tracker({u'member_group': group.cn}, remove=True)
        result = role.retrieve()
        role.check_retrieve(result)

    def test_add_nonexistent_member(self, role):
        """Try to add non-existent group to the role."""
        invalid_group = unicode(str(uuid4()))
        result = role.add_member(invalid_group, member_type='group')
        # manually update tracker since we expect group to not exists
        role.update_tracker({u'member_group': invalid_group}, remove=True)
        role.check_add_nonexistent_member(
            result, invalid_group, member_type='group')

    def test_remove_nonexistent_member(self, role):
        """Try to remove non-existent group from the role."""
        invalid_group = unicode(str(uuid4()))
        result = role.remove_member(invalid_group, member_type='group')
        role.check_remove_member_false(
            result, invalid_group, member_type='group')


class TestRoleFind(XMLRPC_test):
    """Test role-find command using tracker method find_tracker_roles."""
    def test_find_basic(self, role):
        """Test basic find command."""
        role.ensure_exists()
        expected = RoleTracker.find_tracker_roles([role])
        result = role.find(u'test')
        assert_deepequal(expected, result)

    def test_find_similar_roles(self, role, role2):
        """Test find with two roles."""
        role2.ensure_exists()
        expected = RoleTracker.find_tracker_roles(
            [role, role2], pattern=u'test')
        result = role.find(u'test')
        assert_deepequal(expected, result)

    def test_find_with_attributes(self, role, role2, user, privilege):
        """Test find role with privilege and member attributes."""
        privilege.ensure_exists()
        user.ensure_exists()
        role.add_privilege(privilege.cn)
        role2.add_member(user.uid, member_type='user')
        expected = RoleTracker.find_tracker_roles(
            [role, role2], pattern=u'test')
        result = role.find(u'test')
        assert_deepequal(expected, result)

    def test_find_pkey_only(self, role, role2):
        """Find with 'pkey-only' attribute enabled."""
        expected = RoleTracker.find_tracker_roles(
            [role, role2], pattern=u'test', criteria={'pkey_only': True})
        result = role.find(u'test', pkey_only=True)
        assert_deepequal(expected, result)

    def test_find_all(self, role, role2):
        """Find with 'all' attribute enabled."""
        expected = RoleTracker.find_tracker_roles(
            [role, role2], pattern=u'test', criteria={'all': True})
        result = role.find(u'test', all=True)
        assert_deepequal(expected, result)

    def test_find_after_attributes_removal(self, role, role2, user, privilege):
        """Remove attributes set in previous test and run find."""
        role.remove_privilege(privilege.cn)
        role2.remove_member(user.uid, member_type='user')
        expected = RoleTracker.find_tracker_roles(
            [role, role2], pattern=u'test')
        result = role.find(u'test')
        assert_deepequal(expected, result)

    def test_find_by_description(self, role):
        """Find role by valid description and name."""
        expected = RoleTracker.find_tracker_roles(
            [role], pattern=u'test-role1', criteria={'desc': _DESCRIPTION})
        result = role.find(u'test-role1', description=_DESCRIPTION)
        assert_deepequal(expected, result)

    def test_find_by_description_false(self, role):
        """Try to search for role with valid name and random description."""
        invalid_desc = unicode(str(uuid4()))
        expected = RoleTracker.find_tracker_roles(
            [role], pattern=u'test-role1', criteria={'desc': invalid_desc})
        result = role.find(u'test-role1', description=invalid_desc)
        assert_deepequal(expected, result)
