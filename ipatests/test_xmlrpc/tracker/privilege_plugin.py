#
# Copyright (C) 2016  FreeIPA Contributors see COPYING for license
#

from ipalib import api
from ipapython.dn import DN
from ipatests.test_xmlrpc import objectclasses
from ipatests.test_xmlrpc.tracker.base import Tracker
from ipatests.util import assert_deepequal


class PrivilegeTracker(Tracker):
    """Tracker class for privileges."""

    retrieve_keys = {u'cn', u'member'}
    retrieve_all_keys = {u'dn', u'objectclass'}
    create_keys = retrieve_keys | retrieve_all_keys

    def __init__(self, name, **kwargs):
        super(PrivilegeTracker, self).__init__(default_version=None)
        self.cn = name
        self.dn = DN(
            ('cn', self.cn), api.env.container_privilege, api.env.basedn
        )
        self.kwargs = kwargs

    def check_create(self, result, extra_keys=()):
        """Check privilege-add command result."""
        if u'description' in result['result']:
            expected = self.filter_attrs(
                self.create_keys | set(extra_keys) | {u'description'})
        else:
            expected = self.filter_attrs(self.create_keys | set(extra_keys))
        assert_deepequal(dict(
            value=self.cn,
            summary=u'Added privilege "%s"' % self.cn,
            result=self.filter_attrs(expected),
            ), result)

    def check_retrieve(self, result, all=False, raw=False):
        """ Check retrieve-show command result. """
        if all:
            keys = self.retrieve_all_keys
        else:
            keys = self.retrieve_keys
        # description is only returned if exists in IPA role
        if u'description' in result['result']:
            keys = keys | {u'description'}
        expected = self.filter_attrs(keys)
        assert_deepequal(dict(
            value=self.cn,
            summary=None,
            result=expected,
        ), result)

    def check_delete(self, result):
        """Check correct behaviour of 'privilege-del' command."""
        assert_deepequal({
            'value': [self.cn],
            'summary': u'Deleted privilege "%s"' % self.cn,
            'result': {
                'failed': []
            },
        }, result)

    def make_create_command(self, force=None):
        """Make function that creates a privilege using privilege-add."""
        return self.make_command('privilege_add', self.cn, **self.kwargs)

    def make_update_command(self, updates):
        """Make a function that updates a privilege using privilege-mod."""
        return self.make_command('privilege_mod', self.cn, **updates)

    def make_delete_command(self, force=None):
        """Make function that deletes a privilege using privilege-del."""
        return self.make_command('privilege_del', self.cn)

    def make_find_command(self, *args, **kwargs):
        """ Make function that finds privilege using privilege-find """
        return self.make_command('privilege_find', *args, **kwargs)

    def make_retrieve_command(self, rights=False, all=False, raw=False):
        """Make function that retrieves a privilege using privilege-show."""
        return self.make_command(
            'privilege_show', self.cn, rights=rights, raw=raw
        )

    def track_create(self):
        self.attrs = dict(
            dn=self.dn,
            cn=[self.cn],
            objectclass=objectclasses.privilege,
            description=[u'Description string'],
            memberof='defaultpergroup'
        )
        self.exists = True
