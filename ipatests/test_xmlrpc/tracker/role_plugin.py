#
# Copyright (C) 2016  FreeIPA Contributors see COPYING for license
#

from __future__ import print_function

from ipalib import _, api, errors
from ipapython.dn import DN
from ipatests.test_xmlrpc import objectclasses
from ipatests.test_xmlrpc.tracker.base import Tracker
from ipatests.util import assert_deepequal


class RoleTracker(Tracker):
    """ Class for representing Role's tracker state in the system.

    Tracker keeps track of all changes made on role through its methods.
    If you update state of the system related to tested role, e.g. remove
    user or permission from the system, tracker's `attrs` attribute should
    be updated manually for correct behavior.
    """

    retrieve_keys = {
        u'dn', u'cn', u'description', u'privilege',
        u'memberof_privilege', u'member_group', u'member_host',
        u'member_hostgroup', u'member_service',
        u'member_user'
    }
    retrieve_all_keys = retrieve_keys | {u'objectclass'}
    create_keys = retrieve_keys | {u'objectclass'}
    update_keys = retrieve_keys - {u'dn'}
    privilege_keys = retrieve_keys | {u'objectclass'}
    member_keys = retrieve_keys
    primary_keys = {u'cn', u'dn'}

    def __init__(self, name, **kwargs):
        super(RoleTracker, self).__init__(default_version=None)
        self.cn = name
        self.dn = self._create_dn(self.cn)
        self.kwargs = kwargs

    def _create_dn(self, cn):
        return DN(
            ('cn', cn), api.env.container_rolegroup, api.env.basedn
        )

    def add_privilege(self, privilege):
        """Add privilege to the role and update tracker approprietely.

        :param privilege: Name of the privilege to add.
        """
        # calling update with None would delete attribute
        if privilege is not None:
            self.update_tracker({u'memberof_privilege': privilege})
        result = self.run_command(
            'role_add_privilege', self.cn, privilege=privilege)
        return result

    def check_add_privilege(self, result):
        """Check that privilege was correctly added to role.

        :param result: Result of add_privilege command.
        """
        expected = {
            'completed': 1,
            'failed': {
                'member': {
                    'privilege': [],
                },
            },
            'result': self.filter_attrs(self.privilege_keys)
        }
        assert_deepequal(expected, result)

    def check_add_zero_privilege(self, result):
        """Check that adding empty privilege does not change role's state.

        :param result: Result of add_privilege command.
        """
        expected = {
            'completed': 0,
            'failed': {
                'member': {
                    'privilege': [],
                },
            },
            'result': self.filter_attrs(self.privilege_keys)
        }
        assert_deepequal(expected, result)

    def check_add_duplicate_privilege(self, result, privilege):
        """Check that adding duplicat privilege will fail.

        :param result: Result of add_privilege command.
        :param privilege: Name of duplicated privilege.
        """
        expected = {
            'failed': {
                'member': {
                    'privilege': [
                        [privilege, u'This entry is already a member']
                    ]
                }
            },
            'completed': 0,
            'result': self.filter_attrs(self.privilege_keys)
        }
        assert_deepequal(expected, result)

    def check_handle_nonexistent_privilege(self, result, privilege):
        """Check that non-existent privilege will not be added.

        :param result: Result of add_privilege command.
        """
        expected = {
            'completed': 0,
            'failed': {
                'member': {
                    'privilege': [[str(privilege), u'privilege not found']]
                }
            },
            'result': self.filter_attrs(self.privilege_keys)
        }
        assert_deepequal(expected, result)

    def remove_privilege(self, privilege):
        """Remove privilege from role and update tracker.

        :param privilege: Name of the privilege to remove.
        """
        if privilege is not None:
            self.update_tracker(
                {u'memberof_privilege': privilege}, remove=True)
        result = self.run_command(
            'role_remove_privilege', self.cn, privilege=privilege)
        return result

    def check_remove_privilege(self, result):
        """Check correct execution of role-remove-privilege command.

        :param result: Result of remove_privilege command.
        """
        expected = {
            'completed': 1,
            'failed': {
                'member': {
                    'privilege': [],
                },
            },
            'result': self.filter_attrs(self.privilege_keys)
        }
        assert_deepequal(expected, result)

    def check_remove_privilege_false(self, result, privilege):
        """Check that removing privilege not in role will fail.

        :param result: Result of remove_privilege command.
        :param privilege: Name of the non-existing privilege.
        """
        expected = {
            'completed': 0,
            'failed': {
                'member': {
                    'privilege': [(u'%s' % privilege,
                                   u'This entry is not a member')],
                },
            },
            'result': self.filter_attrs(self.privilege_keys)
        }
        assert_deepequal(expected, result)

    def check_remove_zero_privilege(self, result):
        """Check that removing an empty privilege does not change role's state.

        :param privilege: Name of the non-existing privilege.
        """
        expected = {
            'completed': 0,
            'failed': {
                'member': {
                    'privilege': [],
                },
            },
            'result': self.filter_attrs(self.privilege_keys)
        }
        assert_deepequal(expected, result)

    def add_member(self, member, member_type='user'):
        """Add member to this role.

        :param name: Name of the member to add.
        :param member: Type of member to add, can be 'user', 'group',
                       'host', 'hostgroup' or 'service'.
        """
        if member_type not in ('user', 'group', 'host',
                               'hostgroup', 'service'):
            raise TypeError(
                "Invalid member type: %s " % member_type)
        # name that will be used for updating tracker attrs
        result_key = 'member_%s' % member_type
        self.update_tracker({result_key: member})

        result = self.run_command(
            'role_add_member', self.cn, **{member_type: member})
        return result

    def check_add_member(self, result):
        """Check that add-member command was successfull.

        :param result: Result of add_member command.
        """
        expected = {
            'failed': {
                'member': {
                    'user': [],
                    'group': [],
                    'host': [],
                    'hostgroup': [],
                    'service': [],
                },
            },
            'completed': 1,
            'result': self.filter_attrs(self.member_keys)
        }
        assert_deepequal(expected, result)

    def check_add_duplicate_member(self, result, member, member_type):
        """Check that member can not be added to role again.

        :param result: Result of add_member command.
        :param member: Name of duplicated member.
        :param member_type: Name of non-existent member.
        """
        members = {m: [] for m in (
            'host', 'group', 'hostgroup', 'service', 'user')}
        members[member_type] = [[member, u'This entry is already a member']]
        expected = {
            'failed': {
                'member': members
            },
            'completed': 0,
            'result': self.filter_attrs(self.member_keys)
        }
        assert_deepequal(expected, result)

    def check_add_nonexistent_member(self, result, member, member_type):
        """Check that it is not possible to add nonexistent member to role.

        :param result: Result of add_member command.
        :param member: Name of nonexistent member.
        :param member_type: Type of non-existent member.
        """
        members = {m: [] for m in (
            'host', 'group', 'hostgroup', 'service', 'user')}
        members[member_type] = [[member, u'no such entry']]
        expected = {
            'failed': {
                'member': members
            },
            'completed': 0,
            'result': self.filter_attrs(self.member_keys)
        }
        assert_deepequal(expected, result)

    def remove_member(self, member, member_type):
        """Remove specified member from the role.

        :param name: Name of the member to remove.
        :param member: Type of member to remove.
        """
        if member_type not in ('user', 'group',
                               'host', 'hostgroup', 'service'):
            raise TypeError(
                "member: %s is not 'user' nor 'group'" % member_type)
        # name that will be used for updating tracker attrs
        result_key = 'member_%s' % member_type
        self.update_tracker({result_key: member}, remove=True)
        result = self.run_command(
            'role_remove_member', self.cn, **{member_type: member})
        return result

    def check_remove_member(self, result):
        """Check that member was succesfully removed from role.

        :param result: Result to remove_member command.
        """
        expected = {
            'failed': {
                'member':
                {
                    'user': [],
                    'group': [],
                    'host': [],
                    'hostgroup': [],
                    'service': [],
                },
            },
            'completed': 1,
            'result': self.filter_attrs(self.member_keys)
        }
        assert_deepequal(expected, result)

    def check_remove_member_false(self, result, member, member_type='user'):
        """Check that removing member that is not in the role will fail.

        :param name: Name of the member to remove.
        :param member: Type of member to remove.
        """
        members = {m: [] for m in (
            'host', 'group', 'hostgroup', 'service', 'user')}
        members[member_type] = [[member, u'This entry is not a member']]
        expected = {
            'completed': 0,
            'failed': {
                'member': members
            },
            'result': self.filter_attrs(self.member_keys)
        }
        assert_deepequal(expected, result)

    def update_tracker(self, updates, expected_updates=None, remove=False):
        """Helper function to update tracker's attributes.

        This method is used to:
            * add new attribute to self.attrs
            * set new value or add another value to existing attribute
            * remove whole attribute by setting dict value to None, '' or u''
            * remove single value from attribute's list by setting remove=True

        If remove==True and updates contain None, '' or u'', no action
        is performed.
        If remove==True, expected_updates parameter is ignored, since one
        can remove it by simply setting key=None.

        :param updates: Dictionary of attributes that will be stored in list.
        :param expected_updates: Dictionary of other attributes.
        :param remove: Specifies whether to remove value of attribute
                       from list. List will be deleted if empty after removal.
        """
        self.ensure_exists()
        if expected_updates is None:
            expected_updates = {}

        if 'rename' in updates.keys():
            self.attrs['cn'] = [updates['rename']]
            self.attrs['dn'] = self._create_dn(updates['rename'])
            updates.pop('rename')

        if not remove:
            for key, value in updates.items():
                if value is None or value is '' or value is u'':
                    del self.attrs[key]
                else:
                    if type(value) is list:
                        self.attrs[key] = value
                    else:
                        self.attrs[key] = [value]
            for key, value in expected_updates.items():
                if value is None or value is '' or value is u'':
                    del self.attrs[key]
                else:
                    self.attrs[key] = value
        else:
            for key, value in updates.items():
                # if attribute or value is not in attrs, nothing happens...
                try:
                    self.attrs[key].remove(value)
                    if not self.attrs[key]:
                        del self.attrs[key]
                except (KeyError, ValueError):
                    pass

    def update(self, updates):
        """Run update command on role.

        :param updates: Dictionary of attributes to update.
        """
        return self.make_update_command(updates)()

    def check_update(self, result, extra_keys=()):
        """Check correct execution of role_mod command.

        :param result: Result of update command.
        :param extra_keys: Extra keys expected in update result.
        """
        expected = {
            'value': self.cn,
            'summary': u'Modified role "%s"' % self.cn,
            'result': self.filter_attrs(self.update_keys | set(extra_keys))
        }
        assert_deepequal(expected, result)

    def check_create(self, result, extra_keys=()):
        """Check correct behavior of role-add command.

        :param result: Result of create command.
        :param extra_keys: Extra keys expected in create result.
        """
        expected = self.filter_attrs(self.create_keys | set(extra_keys))
        assert_deepequal({
            'value': self.cn,
            'summary': u'Added role "%s"' % self.cn,
            'result': self.filter_attrs(expected),
        }, result)

    def check_delete(self, result):
        """Check correct behaviour of 'role-del' command.

        :param result: Result of delete command.
        """
        assert_deepequal({
            'value': [self.cn],
            'summary': u'Deleted role "%s"' % self.cn,
            'result': {
                'failed': []
            },
        }, result)

    def retrieve(self, rights=False, all=False, raw=False):
        """Check 'role_show' correct behavior.

        :param rights: Display effective rights for attributes.
        :param all: Show detailed description.
        :param raw: Output in raw format.
        """
        result = self.make_retrieve_command(
            rights=rights, all=all, raw=raw)()
        return result

    def check_retrieve(self, result, rights=False, all=False, raw=False):
        """Check correct behavior of role-show command.

        :param result: Result of retrieve command.
        :param rights: Display effective rights for attributes.
        :param all: Show detailed description.
        :param raw: Output in raw format.
        """
        if all:
            keys = self.retrieve_all_keys
        else:
            keys = self.retrieve_keys
        expected = {
            'value': self.cn,
            'summary': None,
            'result': self.filter_attrs(keys),
        }
        assert_deepequal(expected, result)

    def find(self, *args, **kwargs):
        """Perform role-find command on roles.

        :param pattern: Role's name pattern we are searching for.
        :param criteria: Other find criteria.
        """
        return self.make_find_command(*args, **kwargs)()

    @classmethod
    def find_tracker_roles(cls, roles, pattern='', criteria={}):
        """Perform find command on given roles and return result.

        Criteria can be dictionary or kwargs maybe

        :param roles: List of RoleTracker objects we want to perform search on.
        :param patter: Filter by name.
        :param criteria: Other possible criteria, e.g. description.
        """
        matching_roles = []
        keys = cls.retrieve_keys

        for role in roles:
            if not role.exists:
                continue
            # set initially True, change if some attribute do not match
            full_match = True
            if pattern in role.cn:
                for key, value in criteria.items():
                    if key == 'name':
                        if value not in role.attrs['cn']:
                            full_match = False
                            break
                    elif key == 'desc':
                        if value not in role.attrs['description']:
                            full_match = False
                            break
                    elif key == 'pkey_only':
                        keys = cls.primary_keys
                    elif key == 'all':
                        if 'pkey_only' not in criteria.keys():
                            keys = cls.retrieve_all_keys
                    else:
                        raise Exception('Invalid or not implemented criterion')
            else:
                full_match = False
            # role matched, add it to final result
            if full_match:
                matching_roles.append(role)

        # form a final response
        expected = {}
        results = []
        expected[u'count'] = len(matching_roles)
        expected[u'truncated'] = False
        noun = u'role' if expected['count'] == 1 else u'roles'
        expected[u'summary'] = _('%(count)s %(noun)s matched') % {
            'count': str(len(matching_roles)), 'noun': noun}
        for role in matching_roles:
            results.append(role.filter_attrs(keys))
        expected[u'result'] = results
        return expected

    def make_create_command(self, force=None):
        """Make function that creates a role using role-add."""
        return self.make_command('role_add', self.cn, **self.kwargs)

    def make_update_command(self, updates):
        return self.make_command('role_mod', self.cn, **updates)

    def make_delete_command(self, force=None):
        """Make function that deletes a role using role-del."""
        return self.make_command('role_del', self.cn)

    def make_find_command(self, *args, **kwargs):
        """ Make function that finds role using role-find """
        return self.make_command('role_find', *args, **kwargs)

    def make_retrieve_command(self, rights=False, all=False, raw=False):
        """Make function that retrieves a role using role-show."""
        return self.make_command(
            'role_show', self.cn, rights=rights, all=all, raw=raw)

    def track_create(self):
        """Initialize tracker instance."""
        self.attrs = {
            'dn': self.dn,
            'cn': [self.cn],
            'memberof': 'defaultpergroup',
            'objectclass': objectclasses.role,
        }
        for key in self.kwargs:
            if not type(self.kwargs[key]) is list:
                self.attrs[key] = [self.kwargs[key]]
            else:
                self.attrs[key] = self.kwargs[key]
        self.exists = True

    def make_fixture(self, request):
        """Make a pytest fixture for this tracker

        The fixture ensures the plugin entry does not exist before
        and after the tests that use it.
        """
        try:
            self.make_delete_command()()
        except errors.NotFound:
            pass

        def cleanup():
            existed = self.exists
            try:
                self.make_delete_command()()
            except errors.NotFound:
                if existed:
                    raise
            self.exists = False

        request.addfinalizer(cleanup)
        return self
